# kube-client-py

This is a simple python container with the kubernetes package installed.

To be able to work with Kubernetes resources out of a pod, the following should be prepared:
* Role with necessary rights
* ServiceAccount for the Pod
* RoleBinding for the Role and the ServiceAccount

The Serviceaccount should be added to the Pod's configuration. The script can be loaded using a ConfigMap.

