FROM python:3

WORKDIR /usr/src/app

RUN pip3 install kubernetes

COPY . .

CMD ["python", "./script.py"]
